from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import random


def get_photo(city, state):
    """
    Get a random photo from Pexels API for a given city and state.

    Args:
        city (str): Name of the city.
        state (str): Name of the state.

    Returns:
        dict: A dictionary containing the photo details with a 'picture_url' key.
    """
    url = f"https://api.pexels.com/v1/search?query={city}, {state}&per_page=10"
    headers = {"Authorization": f"Bearer {PEXELS_API_KEY}"}

    try:
        response = requests.get(url, headers=headers)
        response.raise_for_status()  # Raise an exception for bad status codes

        data = response.json()

        if data["total_results"] > 0:
            # Choose a random photo from the results
            random_photo = random.choice(data["photos"])

            # Create a dictionary with the picture URL
            return {"picture_url": random_photo["src"]["large"]}
        else:
            print(f"No photos found for {city}, {state}")
            return None

    except requests.exceptions.RequestException as e:
        print(f"Error fetching photo: {e}")
        return None


def get_weather_data(city, state):
    """
    Get weather data from OpenWeatherMap API for a given city.

    Args:
        city (str): Name of the city.
        state (str): Name of the state.

    Returns:
        dict: A dictionary containing the weather details.
    """
    url = f"http://api.openweathermap.org/data/2.5/weather?q={city},{state},US&units=imperial&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)

    if response.status_code == 200:
        weather_data = response.json()
        return {
            "temp": weather_data["main"]["temp"],
            "description": weather_data["weather"][0]["description"],
        }
    else:
        return None
