from django.http import JsonResponse
from django.shortcuts import get_object_or_404
from .models import Conference, Location, State
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
import json
from django.utils import timezone
from .acls import get_photo, get_weather_data


class LocationListEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "state",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]


class LocationDetailEncoder(ModelEncoder):
    model = Location
    properties = [
        "name",
        "city",
        "state",
        "room_count",
        "created",
        "updated",
        "picture_url",
    ]

    def get_extra_data(self, o):
        return {"state": o.state.abbreviation}


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = [
        "id",
        "name",
        "description",
        "max_presentations",
        "max_attendees",
    ]


class ConferenceDetailEncoder(ModelEncoder):
    model = Conference
    properties = [
        "name",
        "description",
        "max_presentations",
        "max_attendees",
        "starts",
        "ends",
        "created",
        "updated",
    ]


@require_http_methods(["GET", "POST"])
def api_list_conferences(request):
    if request.method == "GET":
        conferences = Conference.objects.all()
        return JsonResponse(
            {"conferences": conferences},
            encoder=ConferenceListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location = Location.objects.get(id=content["location"])
            content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        conference = Conference.objects.create(**content)
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_conference(request, id):
    conference = get_object_or_404(Conference, id=id)
    weather = get_weather_data(
        conference.location.city,
        conference.location.state,
    )
    if request.method == "GET":
        conference_data = ConferenceDetailEncoder().default(conference)
        return JsonResponse(
            {"conference": conference_data, "weather": weather},
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = Location.objects.get(id=content["location"])
                content["location"] = location
        except Location.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        for key, value in content.items():
            setattr(conference, key, value)
        conference.updated = timezone.now()
        conference.save()
        return JsonResponse(
            conference,
            encoder=ConferenceDetailEncoder,
            safe=False,
        )
    else:  # DELETE
        conference.delete()
        return JsonResponse(
            {"message": "Conference deleted successfully"},
            status=204,
        )


@require_http_methods(["GET", "POST"])
def api_list_locations(request):
    if request.method == "GET":
        locations = Location.objects.all()
        return JsonResponse(
            {"locations": locations},
            encoder=LocationListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            state = State.objects.get(abbreviation=content["state"])
            content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )

        # Use the city and state's abbreviation in the content dictionary
        # to call the get_photo ACL method

        # Example usage:

        photo_data = get_photo(content["city"], content["state"])
        location = Location.objects.create(**content)
        location.picture_url = photo_data["picture_url"]
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_location(request, id):
    location = get_object_or_404(Location, id=id)
    if request.method == "GET":
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        location.delete()
        return JsonResponse(
            {"message": "Location deleted successfully"},
            status=204,
        )
    else:  # PUT
        content = json.loads(request.body)
        try:
            if "state" in content:
                state = State.objects.get(abbreviation=content["state"])
                content["state"] = state
        except State.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid state abbreviation"},
                status=400,
            )
        for key, value in content.items():
            setattr(location, key, value)
        location.updated = timezone.now()
        location.save()
        return JsonResponse(
            location,
            encoder=LocationDetailEncoder,
            safe=False,
        )
