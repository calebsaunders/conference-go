from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.shortcuts import get_object_or_404
from django.core.exceptions import ValidationError
from django.views.decorators.csrf import csrf_exempt
import json

from .models import Presentation
from common.json import ModelEncoder


class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "presenter_name",
        "company_name",
        "presenter_email",
        "title",
        "synopsis",
        "created",
    ]


@require_http_methods(["GET", "POST"])
@csrf_exempt
def api_list_presentations(request, conference_id):
    """
    Lists the presentations for the specified conference id.
    GET: Returns a list of presentations.
    POST: Creates a new presentation.
    """
    if request.method == "GET":
        presentations = [
            {
                "title": p.title,
                "status": p.status.name,
                "href": p.get_api_url(),
            }
            for p in Presentation.objects.filter(conference=conference_id)
        ]
        return JsonResponse({"presentations": presentations})
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            conference = get_object_or_404(Conference, id=conference_id)
            presentation = Presentation.objects.create(
                conference=conference, **content
            )
            return JsonResponse(
                presentation,
                encoder=PresentationDetailEncoder,
                safe=False,
                status=201,
            )
        except ValidationError as e:
            return JsonResponse({"error": str(e)}, status=400)


@require_http_methods(["GET", "PUT", "DELETE"])
@csrf_exempt
def api_show_presentation(request, id):
    """
    GET: Returns the details of a specific presentation.
    PUT: Updates an existing presentation.
    DELETE: Deletes a presentation.
    """
    presentation = get_object_or_404(Presentation, id=id)

    if request.method == "GET":
        return JsonResponse(
            presentation,
            encoder=PresentationDetailEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            for key, value in content.items():
                setattr(presentation, key, value)
            presentation.save()
            return JsonResponse(
                presentation,
                encoder=PresentationDetailEncoder,
                safe=False,
            )
        except ValidationError as e:
            return JsonResponse({"error": str(e)}, status=400)
    elif request.method == "DELETE":
        presentation.delete()
        return JsonResponse({"deleted": True}, status=204)
