from django.http import JsonResponse, HttpResponseNotFound
from django.views.decorators.http import require_http_methods
from django.core.exceptions import ObjectDoesNotExist
from django.urls import path
from .models import Attendee
from .models import ConferenceVO
from common.json import ModelEncoder
import json


class ConferenceVODetailEncoder(ModelEncoder):
    model = ConferenceVO
    properties = ["name", "import_href"]


class AttendeeListEncoder(ModelEncoder):
    model = Attendee
    properties = ["email", "name"]


class AttendeeDetailEncoder(ModelEncoder):
    model = Attendee
    properties = ["email", "name", "company_name", "created", "conference"]

    def get_extra_data(self, o):
        return {
            "conference": {
                "name": o.conference.name,
                "href": f"/conferences/{o.conference.id}/",
            }
        }


@require_http_methods(["GET", "POST", "PUT"])
def api_list_attendees(request, conference_vo_id=None):
    if request.method == "GET":
        attendees = Attendee.objects.filter(conference=conference_vo_id)
        return JsonResponse(
            {"attendees": attendees},
            encoder=AttendeeListEncoder,
        )
    elif request.method == "POST":
        content = json.loads(request.body)

        try:
            conference_href = f"/api/conferences/{conference_vo_id}/"

            conference = ConferenceVO.objects.get(import_href=conference_href)
            content["conference"] = conference
        except ConferenceVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        attendee = Attendee.objects.create(**content)
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    else:  # PUT
        content = json.loads(request.body)
        try:
            conference = Conference.objects.get(id=conference_id)
        except Conference.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid conference id"},
                status=400,
            )

        for attendee_data in content.get("attendees", []):
            attendee_id = attendee_data.get("id")
            if attendee_id:
                try:
                    attendee = Attendee.objects.get(
                        id=attendee_id, conference=conference
                    )
                    for key, value in attendee_data.items():
                        if key != "id" and hasattr(attendee, key):
                            setattr(attendee, key, value)
                    attendee.save()
                except Attendee.DoesNotExist:
                    return JsonResponse(
                        {
                            "message": f"Attendee with id {attendee_id} not found"
                        },
                        status=400,
                    )
            else:
                attendee_data["conference"] = conference
                Attendee.objects.create(**attendee_data)

        updated_attendees = Attendee.objects.filter(conference=conference)
        return JsonResponse(
            {"attendees": updated_attendees},
            encoder=AttendeeListEncoder,
        )


@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_attendee(request, id):
    try:
        attendee = Attendee.objects.get(id=id)
    except Attendee.DoesNotExist:
        return HttpResponseNotFound({"message": "Attendee not found"})

    if request.method == "GET":
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "PUT":
        content = json.loads(request.body)

        for key, value in content.items():
            setattr(attendee, key, value)
        attendee.save()
        return JsonResponse(
            attendee,
            encoder=AttendeeDetailEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        attendee.delete()
        return JsonResponse({"message": "Attendee deleted"})


# URL patterns (add these to your urls.py)
urlpatterns = [
    path(
        "conferences/<int:conference_id>/attendees/",
        api_list_attendees,
        name="api_list_attendees",
    ),
    path("attendees/<int:id>/", api_show_attendee, name="api_show_attendee"),
]
